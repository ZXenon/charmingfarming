﻿using UnityEngine.EventSystems;
using UnityEngine;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{
	Camera cam;
	PlayerMotor motor;

	public LayerMask movementMask;

	// public Interactable focus;

	// Start is called before the first frame update
	void Start()
	{
		cam = Camera.main;
		motor = GetComponent<PlayerMotor>();

	}
	// Update is called once per frame
	void Update()
	{
		if (EventSystem.current.IsPointerOverGameObject())
		{
			return;
		}

		if (Input.GetMouseButtonDown(0)) // checks for left click
		{
			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 100, movementMask))
			{
				// move player to location
				motor.MoveToPoint(hit.point);

				// stop unfocusing any objects
				//RemoveFocus();
			}
		}
		/*
		if (Input.GetMouseButtonDown(1)) // checks for right click
		{
			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 100))
			{
				Interactable interactable = hit.collider.GetComponent<Interactable>();
				if (interactable != null)
				{
					SetFocus(interactable);
				}
			}
		}
		*/
	}

	/*
	void SetFocus(Interactable newFocus)
	{
		if (newFocus != focus)
		{
			if (focus != null)
			{
				focus.onDefocused();
			}

			focus = newFocus;
			motor.FollowTarget(focus);
		}

		newFocus.OnFocused(transform);
	}

	public void RemoveFocus()
	{
		if (focus != null)
		{
			focus.onDefocused();
		}
		focus = null;
		motor.StopFollowingTarget();
	}
	*/
}
