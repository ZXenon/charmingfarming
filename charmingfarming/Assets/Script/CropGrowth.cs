﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CropGrowth : MonoBehaviour
{
	bool harvestable = false;
	int cropGain = 3;
	int cropStage = 1;

	public GameObject stage1;
	public GameObject stage2;
	public GameObject stage3;
	public GameObject stage4;
	public GameObject stage5;

	float growthTime;
	float Timer = 0;

	public bool fertilized = false;
	public bool watered = false;

	float GrowthAdjust()	//Checks watered and fertilized bools and sets growthTime appropriately
	{
		if ((!watered) && (!fertilized))
			growthTime = 12;
		if ((watered == true) && (!fertilized))
			growthTime = 9;
		if ((!watered) && (fertilized == true))
			growthTime = 8;
		if ((watered == true) && (fertilized == true))
			growthTime = 6;
		return growthTime;
	}

	private void Update()
	{
		GrowthAdjust();
		if(cropStage <= 4)
		{
			Timer += Time.deltaTime;
			if(Timer >= growthTime)
			{
				cropStage += 1;
				Timer = 0;

				Growth(cropStage);
			}
		}
	}

	void Growth (int stage)
	{
		switch (stage)
		{
			case 2:
				stage1.SetActive(false);
				stage2.SetActive(true);
				break;
			case 3:
				stage2.SetActive(false);
				stage3.SetActive(true);
				break;
			case 4:
				stage3.SetActive(false);
				stage4.SetActive(true);
				break;
			case 5:
				stage4.SetActive(false);
				stage5.SetActive(true);
				harvestable = true;
				break;
		}
	}
}
